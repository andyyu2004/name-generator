
pub struct Generator {
    var: u8,
    i: i32
}

impl Generator {
    pub fn new() -> Self {
        Self { var: 0, i: 0 }
    }

    pub fn gen(&mut self) -> String {
        let a = 0x61; // ASCII value of 'a'
        let name = if self.i != 0 { format!("{}{}", (self.var + a) as char, self.i) }
        else { format!("{}", (self.var + a) as char) };

        self.var = (self.var + 1) % 26;
        if self.var == 0 { self.i += 1 }
        name
    }
}

