mod generator;

pub use generator::Generator;

#[cfg(test)]
mod tests {
    use super::generator::Generator;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn test() {
        let mut gen = Generator::new();
        for _ in 0..1000 {
            println!("{}", gen.gen())
        }
    }
}
